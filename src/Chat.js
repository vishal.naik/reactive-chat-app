import React from 'react'
import io from 'socket.io-client';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';

class Chat extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            username: '',
            roomName: '',
            message: '',
            messages: []
        };

        this.socket = io('localhost:3006');

        this.socket.on('RECEIVE_MESSAGE', (msg) => {
            console.log(msg);
            this.setState({
                messages: [...this.state.messages, msg]
            });
        });

        this.socket.on('reconnect', (attemptNumber) => {
            if (this.state.roomName) {
                this.joinRoom();
            }
        });

        this.joinRoom = this.joinRoom.bind(this);
        this.sendMessage = this.sendMessage.bind(this);
    }

    joinRoom() {
        this.socket.emit('JOIN_ROOM', this.state.roomName);
    }

    sendMessage() {
        this.socket.emit('SEND_MESSAGE', {
            name: this.state.username,
            msg: this.state.message
        });

        this.setState({
            messages: [...this.state.messages, {
                name: this.state.username,
                msg: this.state.message
            }],
            message: ''
        });
    }

    render() {
        return (
            <div>
                <h1>V chat</h1>

                <Table style={{width: 185, margin: 'auto'}}>
                    <TableBody>
                        <TableRow>
                            <TableCell style={{width: 185}}>
                                <div style={{height: 160, width: 180, overflow: 'auto'}}>
                                    {this.state.messages.map(msg => {
                                        return <div>{msg.name} : {msg.msg}</div>
                                    })}
                                </div>
                            </TableCell>

                        </TableRow>

                    </TableBody>
                </Table>


                <Table style={{width: 185, margin: 'auto'}}>
                    <TableBody>

                        <TableRow>
                            <TableCell style={{width: 100}}>
                                <TextField required
                                           value={this.state.username}
                                           onChange={event => this.setState({username: event.target.value})}
                                           id='username'
                                           label='Your name'
                                           margin='normal'/>
                            </TableCell>
                        </TableRow>

                        <TableRow>
                            <TableCell style={{width: 100}}>
                                <TextField required
                                           value={this.state.roomName}
                                           onChange={event => this.setState({roomName: event.target.value})}
                                           id='roomName'
                                           label='Room name'
                                           margin='normal'/>

                            </TableCell>

                            <TableCell style={{width: 70}}>
                                <Button variant="contained" color="primary" onClick={this.joinRoom}>Join</Button>
                            </TableCell>
                        </TableRow>


                        <TableRow>
                            <TableCell style={{width: 100}}>
                                <TextField required
                                           value={this.state.message}
                                           onChange={event => this.setState({message: event.target.value})}
                                           id='messageText'
                                           label='Your message here'
                                           margin='normal'/>
                            </TableCell>

                            <TableCell style={{width: 70}}>
                                <Button variant="contained" color="primary" onClick={this.sendMessage}>
                                    Send
                                </Button>
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>

            </div>
        );
    }

}

export default Chat;