'use strict'

const express = require('express');
const socket = require('socket.io');

var numberOfClients = 0;

const app = express();
let server = app.listen(3006, () => {
    console.log('Chat server ready serve your chats.');
});


const io = socket(server);
io.on('connection', (socket) => {
    numberOfClients++;
    console.log('Client connected. ' + numberOfClients);

    socket.on('SEND_MESSAGE', (message) => {
        console.log("Message: " + JSON.stringify(message));
        socket.broadcast.to(socket.roomName).emit('RECEIVE_MESSAGE', message);
    });

    socket.on('JOIN_ROOM', (roomName) => {
        console.log("Joined room " + roomName);
        socket.roomName = roomName;
        socket.join(roomName);
    })


    socket.on('disconnect', () => {
        numberOfClients--;
        console.log("Client is disconnected. " + numberOfClients);
    });
});
